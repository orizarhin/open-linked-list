//
// Created by Ori Zarhin on 14/01/2020.
//

#ifndef HASH_FIX_2_LINKED_LIST_H
#define HASH_FIX_2_LINKED_LIST_H

#include <stdio.h>





template <typename T>
class Linked_list_open {
private:
    //none is private, full user access
public:
    struct Node {
        Node* next;
        Node* prev;
        T* data;

        Node (Node* next = nullptr, Node* prev = nullptr, T* data = nullptr) : next(next) , prev(prev) , data(data) {}
        void set_next(Node* next) { this->next = next; }
        void set_prev(Node* prev) { this->prev = prev; }
        void set_data(T* data) { this->data = data; }
        Node* get_next(Node* next) { return this->next; }
        Node* get_prev(Node* prev) { return this->prev; }
        T* get_data(T* data) { return this->data; }
    };

    Node* root;
    Node* last;
    int size;

    Linked_list_open() : root(nullptr) , last(nullptr) , size(0) {}
    ~Linked_list_open() { //does not delete data
        while (root) {
            Node* temp = root;
            root = root->next;
            delete temp;
        }
    }

    T* delete_root() {
        if (this->root == nullptr) return nullptr;
        T* data = this->root->data;
        Node* temp = this->root;

        if (this->root == this->last) {
            this->last = nullptr;
        }
        this->root = this->root->next;
        if (this->root != nullptr) {
            this->root->prev = nullptr;
        }
        delete temp;
        this->size--;
        return data;
    }

    T* operator--() { //returns the root
        return this->delete_root();
    }

    void insert(T* data) { //done?
        Node *new_node = new Node(nullptr, this->last, data);
        if (this->root == nullptr) {
            this->root = new_node;
        }
        else {
            if (this->last == nullptr) {
                new_node->prev = this->root;
                this->root->next = new_node;
                this->last = new_node;
            }
            else {
                this->last->next = new_node;
                this->last = new_node;
            }
        }
        this->size++;
    }

    T* remove(int k) { //done?
        Node* temp = this->get_node_by_int(k);
        if (temp == nullptr)
            return nullptr;
        T* data = temp->data;
        if (temp == this->root) {
            this->root = this->root->next;
        }
        if (temp == this->last) {
            this->last = temp->prev;
        }
        if (temp->prev != nullptr)
            temp->prev->next = temp->next;
        if (temp->next != nullptr)
            temp->next->prev = temp->prev;

        if (this->root == this->last) {
            this->last = nullptr;
        }
        this->size--;
        delete temp;
        return data;
    }

    T* get_data_by_int(int key) {
        Node* temp = this->get_node_by_int(key);
        if (temp == nullptr)
            return nullptr;
        return temp->data;
    }

    Node* get_node_by_int(int key) {
        Node* temp = this->root;
        for (; temp ; temp = temp->next) {
            if (temp->data != nullptr && **temp->data == key) //ADDED
                return temp;
        }
        return nullptr;
    }


    Linked_list_open& operator+=(Linked_list_open& list) { //uses T's copy constructor of a "T(T*)" declaration --->(server_info(server_info* server))
        Node* temp = list.root;
        for (; temp ; temp = temp->next) {
            this->insert(new T(*temp->data));
        }
        return *this;
    }
};


#endif //HASH_FIX_2_LINKED_LIST_H
